<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Uuid;
use App\Traits\Signature;

class Customer extends Model
{
    use HasFactory;
    // use Uuid, Signature;

    protected $fillable = [

    ];

    protected $dates = ['deleted_at'];
}
